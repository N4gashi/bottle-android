package com.nagashi88.Bottle;

import android.os.Bundle;

public abstract class BaseActivity extends RootActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }
}
