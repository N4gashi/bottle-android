package com.nagashi88.Bottle.discover;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.nagashi88.Bottle.BaseFragment;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.classes.CLBottle;
import com.nagashi88.Bottle.mybottles.MyBottlesActivity;
import com.nagashi88.Bottle.other.MyBottlesListAdapter;
import com.nagashi88.Bottle.other.Top100ListAdapter;
import com.nagashi88.Bottle.tasks.TGetTop100;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Top100Fragment extends BaseFragment
{
    public final static String TOP_100_SELECTED_BOTTLE = "selectedBottleTop100";

    private ListView mBottlesList;
    private ProgressDialog mProgressDialog;
    private RelativeLayout mPlaceholder;

    // region MAIN CALLBACKS

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frg_my_bottles, container, false);

        this.mBottlesList = (ListView)v.findViewById(R.id.my_bottles_list);
        this.mPlaceholder = (RelativeLayout)v.findViewById(R.id.placeholder);
        this.downloadTop100();

        this.mBottlesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                CLBottle currBottle = ((Top100ListAdapter)mBottlesList.getAdapter()).getItem(position);
                Intent in = new Intent(Top100Fragment.this.getActivity(), Top100detailActivity.class);
                in.putExtra(TOP_100_SELECTED_BOTTLE, currBottle);
                startActivity(in);
            }
        });

        return v;
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if(mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    // endregion

    // region CUSTOM METHODS

    public void downloadTop100()
    {
        mProgressDialog = new ProgressDialog(this.getActivity());
        mProgressDialog.setTitle(R.string.loading_title);
        mProgressDialog.setMessage(getString(R.string.loading_bottles));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        new TGetTop100(this, params).execute("/photos/getTop100.json");
    }

    public void onDownloadFinished(JSONObject data) throws JSONException
    {
        List<CLBottle> bottles = new ArrayList<CLBottle>();

        if(mProgressDialog.isShowing())
            mProgressDialog.dismiss();

        JSONArray bottleList = data.getJSONArray("Pictures");
        for(int i = 0; i < bottleList.length(); i++)
        {
            JSONObject jsonBottle = bottleList.getJSONObject(i).getJSONObject("Photo");
            CLBottle bottle = new CLBottle(jsonBottle);
            bottles.add(bottle);
        }

        if(bottles.size() > 0)
            mPlaceholder.setVisibility(View.INVISIBLE);

        this.changeBottlesListAdapter(bottles);
    }

    public void changeBottlesListAdapter(List<CLBottle> bottles)
    {
        Top100ListAdapter adapter = new Top100ListAdapter(this.getActivity(), bottles);
        mBottlesList.setAdapter(adapter);
        mBottlesList.setVisibility(View.VISIBLE);
    }

    // endregion

}
