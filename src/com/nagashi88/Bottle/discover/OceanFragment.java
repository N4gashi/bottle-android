package com.nagashi88.Bottle.discover;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.nagashi88.Bottle.BaseFragment;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.classes.CLBottle;
import com.nagashi88.Bottle.other.OceanListAdapter;
import com.nagashi88.Bottle.tasks.TGetOcean;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OceanFragment extends BaseFragment
{

    public final static String OCEAN_SELECTED_BOTTLE = "selectedBottleOcean";

    private ListView mBottlesList;
    private ProgressDialog mProgressDialog;
    private RelativeLayout mPlaceholder;

    // region MAIN CALLBACKS

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frg_my_bottles, container, false);

        this.mBottlesList = (ListView)v.findViewById(R.id.my_bottles_list);
        this.mPlaceholder = (RelativeLayout)v.findViewById(R.id.placeholder);
        this.downloadTop100();

        this.mBottlesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                CLBottle currBottle = ((OceanListAdapter)mBottlesList.getAdapter()).getItem(position);

                Date date = new Date();
                currBottle.setBottleOpenedDate(date);
                List<CLBottle> list = ((OceanListAdapter) mBottlesList.getAdapter()).getList();
                list.set(position, currBottle);
                ((OceanListAdapter) mBottlesList.getAdapter()).updateData(list);

                Intent in = new Intent(OceanFragment.this.getActivity(), OceanDetailActivity.class);
                in.putExtra(OCEAN_SELECTED_BOTTLE, currBottle);
                startActivity(in);
            }
        });

        return v;
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if(mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    // endregion

    // region CUSTOM METHODS

    public void downloadTop100()
    {
        mProgressDialog = new ProgressDialog(this.getActivity());
        mProgressDialog.setTitle(R.string.loading_title);
        mProgressDialog.setMessage(getString(R.string.loading_bottles));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        new TGetOcean(this, params).execute("/users/getRecievedPictures.json");
    }

    public void onDownloadFinished(JSONObject data) throws JSONException, ParseException
    {
        List<CLBottle> bottles = new ArrayList<CLBottle>();

        if(mProgressDialog.isShowing())
            mProgressDialog.dismiss();

        JSONArray bottleList = data.getJSONArray("Pictures");
        for(int i = 0; i < bottleList.length(); i++)
        {
            JSONObject jsonBottle = bottleList.getJSONObject(i).getJSONObject("Photo");
            JSONObject jsonShare = bottleList.getJSONObject(i).getJSONObject("Share");

            CLBottle bottle = new CLBottle(jsonBottle, jsonShare);
            bottles.add(bottle);
        }

        if(bottles.size() > 0)
            mPlaceholder.setVisibility(View.INVISIBLE);

        this.changeBottlesListAdapter(bottles);
    }

    public void changeBottlesListAdapter(List<CLBottle> bottles)
    {
        OceanListAdapter adapter = new OceanListAdapter(this.getActivity(), bottles);
        mBottlesList.setAdapter(adapter);
        mBottlesList.setVisibility(View.VISIBLE);
    }

    // endregion
}
