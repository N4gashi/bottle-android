package com.nagashi88.Bottle.discover;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.nagashi88.Bottle.BaseActivity;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.other.DiscoverFragmentAdapter;
import com.nagashi88.Bottle.photo.PhotoActivity;

public class DiscoverActivity extends BaseActivity
{

    DiscoverFragmentAdapter mPagerAdapter;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        final ActionBar actionBar = getActionBar();

        setContentView(R.layout.discover);

        this.mPagerAdapter = new DiscoverFragmentAdapter(getSupportFragmentManager());
        this.mViewPager = (ViewPager) findViewById(R.id.pager);
        this.mViewPager.setAdapter(this.mPagerAdapter);
        this.mViewPager.setOffscreenPageLimit(2);
        this.mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
                @Override
                public void onPageSelected(int position)
                {
                    // When swiping between pages, select the
                    // corresponding tab.
                    if(position < 2)
                        getActionBar().setSelectedNavigationItem(position);
                }
        });

        // Specify that tabs should be displayed in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener()
        {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft)
            {
                // When the tab is selected, switch to the
                // corresponding page in the ViewPager.
                mViewPager.setCurrentItem(tab.getPosition());
            }

            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft)
            {
                // hide the given tab
            }

            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft)
            {
                // probably ignore this event
            }
        };

        actionBar.addTab(actionBar.newTab().setText(getString(R.string.tab_top_100)).setTabListener(tabListener));
        actionBar.addTab(actionBar.newTab().setText(getString(R.string.tab_ocean)).setTabListener(tabListener));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_bottles, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_take_pic:
                Intent in = new Intent(this, PhotoActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(in);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
