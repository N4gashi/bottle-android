package com.nagashi88.Bottle.discover;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.loopj.android.image.SmartImageView;
import com.nagashi88.Bottle.BaseActivity;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.classes.CLBottle;
import com.nagashi88.Bottle.classes.CLUser;
import com.nagashi88.Bottle.photo.PhotoActivity;

public class Top100detailActivity extends BaseActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.top100_detail);

        CLBottle bottle = getIntent().getExtras().getParcelable(Top100Fragment.TOP_100_SELECTED_BOTTLE);

        SmartImageView image = (SmartImageView)findViewById(R.id.image);
        TextView title = (TextView)findViewById(R.id.title);
        ImageButton report_button = (ImageButton)findViewById(R.id.report_button);
        ImageButton location_button = (ImageButton)findViewById(R.id.location_button);

        image.setImageUrl(bottle.getUri());
        title.setText(bottle.getName());

        report_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //Yes button clicked
                                
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked

                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(Top100detailActivity.this);
                builder.setMessage(getString(R.string.report_bottle)).setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_bottles, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_take_pic:
                Intent in = new Intent(this, PhotoActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(in);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
