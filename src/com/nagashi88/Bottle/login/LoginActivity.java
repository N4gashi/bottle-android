package com.nagashi88.Bottle.login;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Window;
import com.facebook.Session;
import com.nagashi88.Bottle.BaseActivity;
import com.nagashi88.Bottle.R;

public class LoginActivity extends BaseActivity
{

    private Fragment loginFragment;
    
    // region MAIN CALLBACKS

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);

        if(savedInstanceState == null)
        {
            this.loginFragment = new LoginFragment();
            getSupportFragmentManager()
            .beginTransaction()
            .add(R.id.fragment_container, loginFragment)
            .commit();
        }
        else
        {
            this.loginFragment = getSupportFragmentManager().findFragmentById(android.R.id.content);
        }
    }

    // endregion

}
