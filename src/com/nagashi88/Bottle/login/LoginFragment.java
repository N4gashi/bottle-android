package com.nagashi88.Bottle.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.*;

import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.nagashi88.Bottle.BaseFragment;
import com.nagashi88.Bottle.BottleApplication;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.other.APIHelper;
import com.nagashi88.Bottle.photo.PhotoActivity;
import com.nagashi88.Bottle.tasks.TRegisterLogin;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoginFragment extends BaseFragment
{

    private UiLifecycleHelper uiHelper;
    private boolean loginFired;

    // region MAIN CALLBACKS

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frg_login, container, false);

        LoginButton authButton = (LoginButton) v.findViewById(R.id.authButton);
        authButton.setFragment(this);
        authButton.setReadPermissions(Arrays.asList("basic_info", "email", "user_birthday", "user_location"));

        return v;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        Session session = Session.getActiveSession();

        if (session != null && (session.isOpened() || session.isClosed()))
        {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }



    // endregion

    // region FB CALLBACKS

    private void onSessionStateChange(Session session, SessionState state, Exception exception)
    {
        if (state.isOpened())
        {
            // Logged in
            // Attention, onSessionStateChange peut être déclenchée plus d'une fois !
            this.registerOrLogin(session);
        }
        else if (state.isClosed())
        {
            // Logged out
        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback()
    {
        @Override
        public void call(Session session, SessionState state, Exception exception)
        {
            onSessionStateChange(session, state, exception);
        }
    };

    // endregion

    // region METHODS

    public void registerOrLogin(Session session)
    {
        // facebook_id
        // email
        // username
        // birthdate
        // country
        // about (can be NULL)
        // avatar (can be NULL)
        // cover (can be NULL)

        if(!this.loginFired)
        {
            this.loginFired = true;

            Bundle params = new Bundle();
            params.putString("fields", "birthday,email,name,location");

            new Request(session, "me", params, HttpMethod.GET, new Request.Callback()
            {
                @Override
                public void onCompleted(Response response)
                {
                    GraphUser user = response.getGraphObjectAs(GraphUser.class);
                    BottleApplication app = ((BottleApplication) LoginFragment.this.getActivity().getApplication());
                    app.setFacebookID(user.getId());

                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("User[facebook_id]", app.getFacebookID()));
                    params.add(new BasicNameValuePair("User[email]", user.getProperty("email").toString()));
                    params.add(new BasicNameValuePair("User[username]", user.getName()));

                    DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        params.add(new BasicNameValuePair("User[birthdate]", df2.format(df.parse(user.getBirthday()))));
                    } catch (ParseException e) {
                        APIHelper.log('d', e.getMessage());
                    }

                    params.add(new BasicNameValuePair("User[country]", user.getLocation().getProperty("name").toString()));
                    new TRegisterLogin(LoginFragment.this, params).execute("/users/signin.json");
                }
            }).executeAsync();
        }
    }

    public void registerFinished()
    {
        Intent in = new Intent(this.getActivity(), PhotoActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(in);
    }

    // endregion

}
