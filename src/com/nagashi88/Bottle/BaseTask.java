package com.nagashi88.Bottle;


import android.app.Activity;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import org.apache.http.NameValuePair;

import java.util.List;

public abstract class BaseTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result>
{

    protected Fragment fragment;
    protected String username, pw;
    protected List<NameValuePair> params;

    public BaseTask(Fragment fragment, List<NameValuePair> params)
    {
        this.fragment = fragment;
        this.params = params;

        Activity activity = this.fragment.getActivity();

        this.username = ((BottleApplication)activity.getApplication()).getFacebookID();
        this.pw = this.username;
    }

}