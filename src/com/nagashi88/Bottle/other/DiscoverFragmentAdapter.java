package com.nagashi88.Bottle.other;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.nagashi88.Bottle.discover.OceanFragment;
import com.nagashi88.Bottle.discover.Top100Fragment;

import java.util.List;

public class DiscoverFragmentAdapter extends FragmentStatePagerAdapter
{
    public DiscoverFragmentAdapter(FragmentManager fm)
    {
        super(fm);
    }

    @Override
    public Fragment getItem(int i)
    {
        Fragment fragment = null;

        switch (i)
        {

            case 0:
                fragment = new Top100Fragment();
                break;
            case 1:case 2:
                fragment = new OceanFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
