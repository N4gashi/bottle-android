package com.nagashi88.Bottle.other;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.List;

public class JSONClient
{
    private String credentials;
    private int progressPercent;
    private IProgressPercent asyncTask;

    public JSONClient(String username, String password)
    {
        this.credentials = username + ":" + password;
        this.credentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
    }

    public JSONClient(String username, String password, IProgressPercent asyncTask)
    {
        this(username, password);
        this.asyncTask = asyncTask;
    }

    public JSONData getHttpRequest(String p_uri, List<NameValuePair> params)
    {
        String responseString = null;
        JSONData responseData = null;

        // Create a new HttpClient and Post Header
        String paramsString = URLEncodedUtils.format(params, "UTF-8");
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(p_uri + "?" + paramsString);
        httpget.addHeader("Authorization", "Basic "+this.credentials);

        try {

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httpget);

            // Get response body
            HttpEntity entity = response.getEntity();
            InputStream responseContent = entity.getContent();

            // Progress percent
            //this.processPercent(responseContent, response);

            // Response formatting
            //responseString = this.getResponseFromContentInputString(responseContent, "UTF-8");
            responseString = this.processPercent(responseContent, response);
            //APIHelper.log('d', responseString);
            JSONObject JSONresult = new JSONObject(responseString);

            responseData = new JSONData(response.getStatusLine().getStatusCode(), JSONresult);


        }
        catch (JSONException e)
        {
            APIHelper.log('e', e.getMessage());
        }
        catch (IOException e) {
            //e.printStackTrace();
            APIHelper.log('e', e.getMessage());
        }

        return responseData;
    }

    public JSONData postHttpRequest(String p_uri, List<NameValuePair> params)
    {
        String responseString = null;
        JSONData responseData = null;

        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(p_uri);

        // Add your data
        try {
            httppost.setEntity(new UrlEncodedFormEntity(params));
            httppost.addHeader("Authorization", "Basic "+this.credentials);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            // Get response body
            HttpEntity entity = response.getEntity();

            responseString = EntityUtils.toString(entity, "UTF-8");
            //APIHelper.log('d', responseString);
            JSONObject JSONresult = new JSONObject(responseString);

            responseData = new JSONData(response.getStatusLine().getStatusCode(), JSONresult);

        }
        catch (JSONException e)
        {
            APIHelper.log('e', e.getMessage());
        }
        catch (IOException e)
        {
            APIHelper.log('e', e.getMessage());
        }

        return responseData;
    }

    public JSONData multipartPostHttpRequest(String p_uri, List<NameValuePair> params, MultipartEntity multipartEntity)
    {
        String responseString = null;
        JSONData responseData = null;

        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(p_uri);

        // Add your data
        httppost.setEntity(multipartEntity);
        httppost.addHeader("Authorization", "Basic "+this.credentials);

        try {

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);

            // Get response body
            HttpEntity entity = response.getEntity();

            responseString = EntityUtils.toString(entity, "UTF-8");
            //APIHelper.log('d', responseString);
            JSONObject JSONresult = new JSONObject(responseString);

            responseData = new JSONData(response.getStatusLine().getStatusCode(), JSONresult);

        }
        catch (JSONException e)
        {
            APIHelper.log('e', e.getMessage());
        }
        catch (IOException e)
        {
            APIHelper.log('e', e.getMessage());
        }

        APIHelper.log('d', responseString);
        return responseData;
    }

    private String getResponseFromContentInputString(InputStream is, String encoding) throws IOException
    {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, encoding));
        StringBuilder stringBuilder = new StringBuilder();

        String responseBody;
        while((responseBody = bufferedReader.readLine()) != null)
        {
            stringBuilder.append(responseBody);
        }

        return stringBuilder.toString();
    }

    private String processPercent(InputStream content, HttpResponse response) throws IOException
    {
        InputStream in = content;
        InputStreamReader streamReader = new InputStreamReader(in, "UTF-8");
        StringBuilder stringBuilder = new StringBuilder();

        int totalBytes  = Integer.parseInt(response.getFirstHeader("Content-Length").getValue());
        int processedByte;
        int loaded = 0;
        int prevPercent = -1;
        StringBuffer sb = new StringBuffer();
        //APIHelper.log('d', "MPQ.JSONClient.processPercent", totalBytes);

        while((processedByte = streamReader.read()) != -1)
        {
            //APIHelper.log('d', "MPQ.JSONClient.processPercent", "Boucle");

            //sb.append((char) processedByte);
            stringBuilder.append((char)processedByte);

            if(this.asyncTask instanceof IProgressPercent)
            {
                //APIHelper.log('d', "MPQ.JSONClient:processPercent", "processed : " + processedByte);

                //lastProcessed = processedByte;
                //i++;
                //loaded += processedByte;
                ++loaded;
                float percent = ((100 * loaded) / totalBytes);
                this.progressPercent = (int) percent;

                if(this.progressPercent != prevPercent)
                {
                    prevPercent = this.progressPercent;
                    this.progressPercent = (int) percent;
                    this.asyncTask.doProgress(this.progressPercent);
                }

                //APIHelper.log('d', "MPQ.JSONClient:processPercent", "loaded : " + loaded);
                //APIHelper.log('d', "MPQ.JSONClient:processPercent", "first : " + first);
                //APIHelper.log('d', "MPQ.JSONClient:processPercent", "tota : " + totalBytes);
                //APIHelper.log('d', "MPQ.JSONClient:processPercent", "percent : " + percent);

            }
        }

        streamReader.close();

        return stringBuilder.toString();
    }
}
