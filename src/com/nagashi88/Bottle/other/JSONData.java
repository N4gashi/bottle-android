package com.nagashi88.Bottle.other;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONData
{
    private int httpCode;
    private Object response;

    public JSONData(int httpCode, JSONObject response)
    {
        this.httpCode = httpCode;
        this.response = response;
    }

    public int getHttpCode()
    {
        return this.httpCode;
    }

    public Object getResponse()
    {
        return response;
    }

    public void setResponse(JSONObject response)
    {
        this.response = response;
    }

    public void setResponse(JSONArray response) { this.response = response; }

    @Override
    public String toString()
    {
        return "Status : " + this.getHttpCode() + " Response " + this.getResponse();
    }
}
