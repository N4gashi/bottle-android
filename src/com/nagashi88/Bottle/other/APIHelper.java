package com.nagashi88.Bottle.other;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import com.facebook.Session;
import com.nagashi88.Bottle.R;
import org.apache.http.HttpStatus;

import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class APIHelper
{
    public static final int MODE_DEBUG = 1;
    public static final String LOG_TAG = "Bottle";

    public static final String BASE_URL = "http://www.8milestoheaven.net/BottleAPI";
    public static final String USER_PHOTOS_PATH = "user_photos";

    public static void log(char level, Object msg)
    {

        String className = Thread.currentThread().getStackTrace()[3].getFileName();
        className = className.substring(0, className.length()-5);
        String tag = LOG_TAG + "." + className + "." + Thread.currentThread().getStackTrace()[3].getMethodName();
        String message = (msg instanceof String) ? (String)msg : String.valueOf(msg);

        if(APIHelper.MODE_DEBUG == 1)
        {
            if(level == 'v')
            {
                Log.v(tag, message);
            }
            else if(level == 'd')
            {
                Log.d(tag, message);
            }
            else if(level == 'i')
            {
                Log.i(tag, message);
            }
            else if(level == 'w')
            {
                Log.w(tag, message);
            }
            else if(level == 'e')
            {
                Log.e(tag, message);
            }
            else
            {
                Log.wtf(tag, message);
            }
        }
    }

    public static void makeToast(Context context, String msg, int length)
    {
        Toast toast = Toast.makeText(context, msg, length);
        toast.show();
    }

    public static void makeToast(Context context, String msg)
    {
        makeToast(context, msg, Toast.LENGTH_SHORT);
    }

    public static boolean isNetworkConnected(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static boolean handleStatusCodes(Context context, int code)
    {
        if(code == HttpStatus.SC_OK)
        {
            return true;
        }
        else if(code == HttpStatus.SC_UNAUTHORIZED)
        {
            APIHelper.makeToast(context, context.getResources().getString(R.string.unauthorized_code));
        }
        else
        {
            APIHelper.makeToast(context, context.getResources().getString(R.string.server_error));
        }

        return false;
    }

    public static String ucfirst(String chaine)
    {
        return chaine.substring(0, 1).toUpperCase()+ chaine.substring(1).toLowerCase();
    }

    public static String getCurrentActivity(Activity ac)
    {
        ActivityManager am = (ActivityManager) ac.getSystemService(ac.getApplicationContext().ACTIVITY_SERVICE);
        // get the info from the currently running task
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        ComponentName componentInfo = taskInfo.get(0).topActivity;
        return componentInfo.getShortClassName();
    }

    public static Bitmap getBitmap(String uri) throws IOException
    {
        URL url = new URL(uri);
        Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        return image;
    }

    public static Uri getInternalImageUri(Context context, String filename)
    {
        ContextWrapper cw = new ContextWrapper(context);
        File dir = cw.getDir(APIHelper.USER_PHOTOS_PATH, Context.MODE_PRIVATE);
        File mypath = new File(dir, filename);
        Uri uri = Uri.parse(mypath.getPath());
        return uri;
    }

    public static Bitmap getInternalImageBitmap(Context context, String filename)
    {
        ContextWrapper cw = new ContextWrapper(context);
        File dir = cw.getDir(APIHelper.USER_PHOTOS_PATH, Context.MODE_PRIVATE);
        File mypath = new File(dir, filename);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(mypath.getAbsolutePath(), options);

        return bitmap;
    }

    public static String saveToInternalSorage(Context context, Bitmap bitmapImage, String filename)
    {
        ContextWrapper cw = new ContextWrapper(context);

        // Path to /data/data/yourapp/app_data/user_photos
        File directory = cw.getDir(APIHelper.USER_PHOTOS_PATH, Context.MODE_PRIVATE);
        // Create imageDire
        File mypath = new File(directory, filename+".jpg");
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return directory.getAbsolutePath();
    }


    public static boolean isLoggedInToFacebook(Session session)
    {
        if (session != null && session.isOpened())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static int minutesDiff(Date earlierDate, Date laterDate)
    {
        if( earlierDate == null || laterDate == null ) return 0;
        return (int)((laterDate.getTime()/60000) - (earlierDate.getTime()/60000));
    }

}
