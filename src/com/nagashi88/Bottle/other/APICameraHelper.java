package com.nagashi88.Bottle.other;


import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;

public class APICameraHelper
{

    public static boolean isCameraHardware(Context context)
    {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static Camera getInstance()
    {
        int cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        return getInstance(cameraId);
    }

    public static Camera getInstance(int cameraId)
    {
        Camera c = null;

        try {
            c = Camera.open(cameraId); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            APIHelper.log('e', e.getMessage());
        }

        return c; // returns null if camera is unavailable
    }



}
