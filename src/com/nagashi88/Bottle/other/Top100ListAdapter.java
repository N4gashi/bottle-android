package com.nagashi88.Bottle.other;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.loopj.android.image.SmartImageView;
import com.nagashi88.Bottle.BaseActivity;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.classes.CLBottle;

import java.util.List;

public class Top100ListAdapter extends BaseAdapter
{

    List<CLBottle> bottles;
    private static LayoutInflater inflater = null;

    public Top100ListAdapter(Activity activity, List<CLBottle> bottles)
    {
        this.inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bottles = bottles;
    }

    public int getCount()
    {
        return bottles.size();
    }

    public CLBottle getItem(int position)
    {
        return bottles.get(position);
    }

    public long getItemId(int position)
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        View vi = convertView;

        if(convertView == null)
        {
            vi = inflater.inflate(R.layout.item_bottle_top100, null);
        }

        CLBottle bottle = this.bottles.get(position);

        SmartImageView image = (SmartImageView)vi.findViewById(R.id.image);
        TextView title = (TextView)vi.findViewById(R.id.title);
        TextView rank = (TextView)vi.findViewById(R.id.rank);

        image.setImageUrl(bottle.getUri());
        title.setText(bottle.getName());
        rank.setText(Integer.toString(position+1));

        return vi;

    }

    public void updateData(List<CLBottle> bottles)
    {
        this.bottles = bottles;
        notifyDataSetChanged();
    }

}
