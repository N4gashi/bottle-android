package com.nagashi88.Bottle.other;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback
{

    private SurfaceHolder mHolder;
    private Camera mCamera;
    private Activity mActivity;

    private List<Camera.Size> mSupportedPreviewSizes;

    private int currCamera = Camera.CameraInfo.CAMERA_FACING_BACK;
    private static boolean flashEnabled = false;

    public CameraPreview(Activity activity, Camera camera)
    {
        super(activity);
        this.mCamera = camera;
        this.mActivity = activity;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        this.mHolder = getHolder();
        this.mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        this.mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder)
    {
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            APIHelper.log('d', "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder)
    {
        if(mCamera != null)
        {
            // Call stopPreview() to stop updating the preview surface.
            mCamera.stopPreview();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null)
        {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        this.setAccurateCameraOrientation();

        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

        mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
        Camera.Size mPreviewSize = getOptimalSize(mSupportedPreviewSizes, w, h);
        parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
        parameters.setPictureSize(mPreviewSize.width, mPreviewSize.height);

        requestLayout();
        mCamera.setParameters(parameters);

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e){
            APIHelper.log('d', "Error starting camera preview: " + e.getMessage());
        }
    }

    public void setCamera(Camera camera)
    {
        if(mCamera == camera){ return; }

        this.stopPreviewAndFreeCamera();

        mCamera = camera;

        if(mCamera != null)
        {
            this.setAccurateCameraOrientation();
            List<Camera.Size> localSizes = mCamera.getParameters().getSupportedPreviewSizes();
            mSupportedPreviewSizes = localSizes;
            requestLayout();

            try {
                mCamera.setPreviewDisplay(mHolder);
            } catch (IOException e) {
                APIHelper.log('e', e.getMessage());
            }

            // Important: Call startPreview() to start updating the preview
            // surface. Preview must be started before you can take a picture.
            mCamera.startPreview();
        }
    }

    public boolean toggleFlashled()
    {
        Camera.Parameters params = this.mCamera.getParameters();

        if(flashEnabled)
        {
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        }
        else
        {
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        }

        this.mCamera.setParameters(params);
        flashEnabled = !flashEnabled;

        return flashEnabled;
    }

    public void toggleCamera()
    {

        this.stopPreviewAndFreeCamera();

        if(this.currCamera == Camera.CameraInfo.CAMERA_FACING_BACK)
        {
            this.currCamera = Camera.CameraInfo.CAMERA_FACING_FRONT;
        }
        else
        {
            this.currCamera = Camera.CameraInfo.CAMERA_FACING_BACK;
        }

        Camera camera = Camera.open(this.currCamera);
        this.setCamera(camera);
    }

    public void setAccurateCameraOrientation()
    {
        Camera.Parameters params = this.mCamera.getParameters();

        // STEP 1: Get rotation degrees
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(this.currCamera, info);
        int rotation = this.mActivity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation)
        {
            case Surface.ROTATION_0: degrees = 0; break; //Natural orientation
            case Surface.ROTATION_90: degrees = 90; break; //Landscape left
            case Surface.ROTATION_180: degrees = 180; break;//Upside down
            case Surface.ROTATION_270: degrees = 270; break;//Landscape right
        }

        int rotate = 0;

        if(this.currCamera == Camera.CameraInfo.CAMERA_FACING_BACK)
        {
            rotate = (info.orientation - degrees + 360) % 360;
        }
        else
        {
            rotate = (info.orientation - degrees + 180) % 180;
        }

        // STEP 2: Set the rotation parameter
        params.setRotation(rotate);
        this.mCamera.setDisplayOrientation(rotate);
        this.mCamera.setParameters(params);
    }

    private Camera.Size getOptimalSize(List<Camera.Size> sizes, int w, int h)
    {

        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)h / w;
        if (sizes == null) return null;
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public void stopPreviewAndFreeCamera()
    {

        if (mCamera != null)
        {
            // Call stopPreview() to stop updating the preview surface.
            mCamera.stopPreview();

            mCamera.setPreviewCallback(null);
            mHolder.removeCallback(this);

            // Important: Call release() to release the camera for use by other
            // applications. Applications should release the camera immediately
            // during onPause() and re-open() it during onResume()).
            mCamera.release();

            mCamera = null;
        }
    }


}
