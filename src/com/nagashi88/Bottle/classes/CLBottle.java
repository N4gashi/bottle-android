package com.nagashi88.Bottle.classes;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CLBottle implements Parcelable
{

    private final String BASE_UPLOAD_PATH = "http://8milestoheaven.net/BottleAPI/uploads/";

    private int id;
    private String name, uri;

    private Date bottleOpenedDate;
    private int bottleSharedState;

    public CLBottle(JSONObject data) throws JSONException
    {
        this.id = data.getInt("id");
        this.name = (data.isNull("about")) ? "" : data.getString("about");
        this.uri = data.getString("uri");
    }

    public CLBottle(JSONObject data, JSONObject shareData) throws JSONException, ParseException
    {
        this(data);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.bottleOpenedDate = (!shareData.isNull("date_opened")) ? df.parse(shareData.getString("date_opened")) : null;
        this.bottleSharedState = shareData.optInt("shared", 0);
    }

    // region GETTERS

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUri() {
        return BASE_UPLOAD_PATH+uri;
    }

    public Date getBottleOpenedDate()
    {
        return bottleOpenedDate;
    }

    public int getBottleSharedState()
    {
        return bottleSharedState;
    }

    // endregion

    // region SETTERS

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setBottleOpenedDate(Date bottleOpenedDate)
    {
        this.bottleOpenedDate = bottleOpenedDate;
    }

    // endregion

    // region PARCELABLE

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.uri);
    }

    public static final Parcelable.Creator<CLBottle> CREATOR = new Parcelable.Creator<CLBottle>()
    {
        @Override
        public CLBottle createFromParcel(Parcel source)
        {
            return new CLBottle(source);
        }

        @Override
        public CLBottle[] newArray(int size)
        {
            return new CLBottle[size];
        }
    };

    public CLBottle(Parcel in)
    {
        this.id = in.readInt();
        this.name = in.readString();
        this.uri = in.readString();
    }

    // endregion

}
