package com.nagashi88.Bottle.classes;

import com.nagashi88.Bottle.other.JSONData;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CLUser
{

    private String facebookID, email, username, country;
    private Date birthdate;
    private boolean active;

    public static final String CLUSER_FACEBOOK_ID_FIELD = "facebook_id";
    public static final String CLUSER_EMAIL_FIELD = "email";
    public static final String CLUSER_USERNAME_FIELD = "username";
    public static final String CLUSER_BIRTHDATE_FIELD = "birthdate";
    public static final String CLUSER_COUNTRY_FIELD = "country";
    public static final String CLUSER_ACTIVE_FIELD = "active";

    public CLUser(String facebookID, String email, String username, Date birthdate, String country, boolean active)
    {
        this.facebookID = facebookID;
        this.email = email;
        this.username = username;
        this.birthdate = birthdate;
        this.country = country;
        this.active = active;
    }

    public CLUser(JSONObject response) throws ParseException
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        this.facebookID = response.optString(CLUSER_FACEBOOK_ID_FIELD);
        this.email = response.optString(CLUSER_EMAIL_FIELD);
        this.username = response.optString(CLUSER_USERNAME_FIELD);
        this.birthdate = df.parse(response.optString(CLUSER_BIRTHDATE_FIELD));
        this.country = response.optString(CLUSER_COUNTRY_FIELD);
        this.active = response.optBoolean(CLUSER_ACTIVE_FIELD);
    }

    // region GETTERS

    public String getFacebookID(){
        return facebookID;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getCountry() {
        return country;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public boolean isActive() {
        return active;
    }

    // endregion

    // region SETTERS

    public void setFacebookID(String facebookID) {
        this.facebookID = facebookID;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    // endregion

}
