package com.nagashi88.Bottle.tasks;

import android.support.v4.app.Fragment;
import com.nagashi88.Bottle.BaseTask;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.discover.Top100Fragment;
import com.nagashi88.Bottle.other.APIHelper;
import com.nagashi88.Bottle.other.JSONClient;
import com.nagashi88.Bottle.other.JSONData;
import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class TReportPicture extends BaseTask<String, Integer, JSONData>
{

    public TReportPicture(Fragment fragment, List<NameValuePair> params)
    {
        super(fragment, params);
    }

    protected JSONData doInBackground(String... url)
    {
        url[0] = APIHelper.BASE_URL + url[0];

        JSONClient client = new JSONClient(this.username, this.pw);
        JSONData response = client.getHttpRequest(url[0], params);

        return response;
    }

    protected void onPostExecute(JSONData result)
    {
        JSONObject jsonObject = (JSONObject)result.getResponse();
        if(result == null)
        {
            APIHelper.makeToast(fragment.getActivity(), fragment.getActivity().getResources().getString(R.string.server_error));
        }
        else
        {

            if(APIHelper.handleStatusCodes(this.fragment.getActivity(), result.getHttpCode()))
            {
                APIHelper.makeToast(fragment.getActivity(), "Bottle report done.");
            }
        }
    }

}
