package com.nagashi88.Bottle.tasks;

import android.support.v4.app.Fragment;
import android.util.Log;
import com.nagashi88.Bottle.BaseTask;
import com.nagashi88.Bottle.BottleApplication;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.classes.CLUser;
import com.nagashi88.Bottle.login.LoginFragment;
import com.nagashi88.Bottle.other.APIHelper;
import com.nagashi88.Bottle.other.JSONClient;
import com.nagashi88.Bottle.other.JSONData;
import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.List;

public class TRegisterLogin extends BaseTask<String, Integer, JSONData>
{

    public TRegisterLogin(Fragment fragment, List<NameValuePair> params)
    {
        super(fragment, params);
    }

    protected JSONData doInBackground(String... url)
    {
        url[0] = APIHelper.BASE_URL + url[0];

        JSONClient client = new JSONClient(this.username, this.pw);
        JSONData response = client.postHttpRequest(url[0], params);

        return response;
    }

    protected void onPostExecute(JSONData result)
    {
        if(result == null)
        {
            APIHelper.makeToast(fragment.getActivity(), fragment.getActivity().getResources().getString(R.string.server_error));
        }
        else
        {
            if(APIHelper.handleStatusCodes(this.fragment.getActivity(), result.getHttpCode()))
            {
                try {
                    JSONObject userObject = ((JSONObject) result.getResponse()).getJSONObject("User");
                    CLUser user = new CLUser(userObject);
                    ((BottleApplication)this.fragment.getActivity().getApplication()).setMe(user);
                    ((LoginFragment)this.fragment).registerFinished();
                }
                catch (JSONException e){
                    APIHelper.log('e', e.getMessage());
                }
                catch (ParseException e) {
                    APIHelper.log('e', e.getMessage());
                }
            }
        }
    }

}
