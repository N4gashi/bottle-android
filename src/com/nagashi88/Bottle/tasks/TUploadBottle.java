package com.nagashi88.Bottle.tasks;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import com.nagashi88.Bottle.BaseTask;
import com.nagashi88.Bottle.BottleApplication;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.login.LoginFragment;
import com.nagashi88.Bottle.other.APIHelper;
import com.nagashi88.Bottle.other.JSONClient;
import com.nagashi88.Bottle.other.JSONData;
import com.nagashi88.Bottle.photo.SendFragment;
import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.List;

public class TUploadBottle extends BaseTask<String, Integer, JSONData>
{
    Bitmap photo;
    String imageTitle;

    public TUploadBottle(Fragment fragment, List<NameValuePair> params, Bitmap bm, String imageTitle)
    {
        super(fragment, params);
        this.photo = bm;
        this.imageTitle = imageTitle;
    }

    protected JSONData doInBackground(String... url)
    {
        url[0] = APIHelper.BASE_URL + url[0];

        // Convert photo
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        this.photo.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] data = bos.toByteArray();
        ByteArrayBody bab = new ByteArrayBody(data, "forest.jpg");
        MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        reqEntity.addPart("file", bab);
        try {
            reqEntity.addPart("Photo[about]", new StringBody(this.imageTitle));
        } catch (UnsupportedEncodingException e) {
            APIHelper.log('e', e.getMessage());
        }

        JSONClient client = new JSONClient(this.username, this.pw);
        JSONData response = client.multipartPostHttpRequest(url[0], params, reqEntity);

        return response;
    }

    protected void onPostExecute(JSONData result)
    {
        boolean success = false;

        if(result == null)
        {
            APIHelper.makeToast(fragment.getActivity(), fragment.getActivity().getResources().getString(R.string.server_error));
        }
        else
        {

            if(APIHelper.handleStatusCodes(this.fragment.getActivity(), result.getHttpCode()))
            {
                success = true;
            }
        }

        ((SendFragment)this.fragment).uploadFinished(success);
    }
}
