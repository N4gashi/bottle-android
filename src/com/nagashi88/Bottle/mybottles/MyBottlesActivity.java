package com.nagashi88.Bottle.mybottles;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.nagashi88.Bottle.BaseActivity;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.classes.CLBottle;
import com.nagashi88.Bottle.photo.PhotoActivity;

public class MyBottlesActivity extends BaseActivity
{

    public final static String BOTTLE_OBJECT = "bottleObject";
    private Fragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        if(savedInstanceState == null)
        {
            this.mFragment = new MyBottlesFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, this.mFragment)
                    .commit();
        }
        else
        {
            this.mFragment = getSupportFragmentManager().findFragmentById(android.R.id.content);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_bottles, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_take_pic:
                Intent in = new Intent(this, PhotoActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(in);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void goToDetailScreen(CLBottle bottle)
    {
        Fragment newFragment = new MyBottlesDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(this.BOTTLE_OBJECT, bottle);
        newFragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_container, newFragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
