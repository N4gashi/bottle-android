package com.nagashi88.Bottle.mybottles;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.loopj.android.image.SmartImageView;
import com.nagashi88.Bottle.BaseFragment;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.classes.CLBottle;

public class MyBottlesDetailFragment extends BaseFragment
{

    private CLBottle currentBottle;

    // region MAIN CALLBACKS

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frg_my_bottles_detail, container, false);

        this.currentBottle = this.getArguments().getParcelable(MyBottlesActivity.BOTTLE_OBJECT);

        SmartImageView image = (SmartImageView)v.findViewById(R.id.image);
        TextView title = (TextView)v.findViewById(R.id.title);
        ImageButton button = (ImageButton)v.findViewById(R.id.location_button);

        image.setImageUrl(this.currentBottle.getUri());
        title.setText(this.currentBottle.getName());

        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });

        return v;
    }

    // endregion

}
