package com.nagashi88.Bottle.mybottles;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.nagashi88.Bottle.BaseFragment;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.classes.CLBottle;
import com.nagashi88.Bottle.other.MyBottlesListAdapter;
import com.nagashi88.Bottle.tasks.TGetOwnPictures;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyBottlesFragment extends BaseFragment
{

    private ListView mBottlesList;
    private ProgressDialog mProgressDialog;
    private RelativeLayout mPlaceholder;

    // region MAIN CALLBACKS

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frg_my_bottles, container, false);

        this.mBottlesList = (ListView)v.findViewById(R.id.my_bottles_list);
        this.mPlaceholder = (RelativeLayout)v.findViewById(R.id.placeholder);
        this.downloadMyBottles();

        this.mBottlesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                CLBottle currBottle = ((MyBottlesListAdapter)mBottlesList.getAdapter()).getItem(position);
                ((MyBottlesActivity)MyBottlesFragment.this.getActivity()).goToDetailScreen(currBottle);
            }
        });

        return v;
    }

    // endregion

    // region CUSTOM FUNCTIONS

    public void downloadMyBottles()
    {
        mProgressDialog = new ProgressDialog(this.getActivity());
        mProgressDialog.setTitle(R.string.loading_title);
        mProgressDialog.setMessage(getString(R.string.loading_bottles));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        new TGetOwnPictures(this, params).execute("/users/getOwnPictures.json");
    }

    public void onDownloadFinished(JSONObject data) throws JSONException
    {
        List<CLBottle> bottles = new ArrayList<CLBottle>();

        if(mProgressDialog.isShowing())
            mProgressDialog.dismiss();

        JSONArray bottleList = data.getJSONArray("Pictures");
        for(int i = 0; i < bottleList.length(); i++)
        {
            JSONObject jsonBottle = bottleList.getJSONObject(i).getJSONObject("Photo");
            CLBottle bottle = new CLBottle(jsonBottle);
            bottles.add(bottle);
        }

        if(bottles.size() > 0)
            mPlaceholder.setVisibility(View.INVISIBLE);

        this.changeBottlesListAdapter(bottles);
    }

    public void changeBottlesListAdapter(List<CLBottle> bottles)
    {
        MyBottlesListAdapter adapter = new MyBottlesListAdapter(this.getActivity(), bottles);
        mBottlesList.setAdapter(adapter);
        mBottlesList.setVisibility(View.VISIBLE);
    }

    // endregion
}
