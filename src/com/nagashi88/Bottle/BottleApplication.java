package com.nagashi88.Bottle;

import android.app.Application;
import android.content.res.Configuration;
import com.nagashi88.Bottle.classes.CLUser;

public class BottleApplication extends Application
{

    private static BottleApplication instance;
    private String facebookID;
    private CLUser me;

    public BottleApplication getInstance()
    {
        return instance;
    }

    // region APPLICATION CALLBACKS

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
    }

    @Override
    public void onTerminate()
    {
        super.onTerminate();
    }

    // endregion

    // region GETTERS

    public String getFacebookID()
    {
        return this.facebookID;
    }

    public CLUser getMe()
    {
        return me;
    }

    // endregion

    // region SETTERS

    public void setFacebookID(String fbID)
    {
        this.facebookID = fbID;
    }

    public void setMe(CLUser me)
    {
        this.me = me;
    }

    // endregion
}
