package com.nagashi88.Bottle;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public abstract class RootActivity extends FragmentActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }
}
