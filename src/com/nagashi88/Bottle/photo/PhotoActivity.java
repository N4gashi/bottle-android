package com.nagashi88.Bottle.photo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;
import com.nagashi88.Bottle.BaseActivity;
import com.nagashi88.Bottle.R;


public class PhotoActivity extends BaseActivity
{

    private Fragment takeFragment;

    public static final String PHOTO_TAKEN_URI = "photoTakenURI";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);

        if(savedInstanceState == null)
        {
            this.takeFragment = new TakeFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, takeFragment)
                    .commit();
        }
        else
        {
            this.takeFragment = getSupportFragmentManager().findFragmentById(android.R.id.content);
        }
    }

    public void goToTitleScreen(String photoPath)
    {
        Fragment newFragment = new SendFragment();
        Bundle args = new Bundle();
        args.putString(this.PHOTO_TAKEN_URI, photoPath);
        newFragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.fragment_container, newFragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
