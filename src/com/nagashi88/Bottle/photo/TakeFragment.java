package com.nagashi88.Bottle.photo;

import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.nagashi88.Bottle.BaseFragment;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.discover.DiscoverActivity;
import com.nagashi88.Bottle.mybottles.MyBottlesActivity;
import com.nagashi88.Bottle.other.APICameraHelper;
import com.nagashi88.Bottle.other.APIHelper;
import com.nagashi88.Bottle.other.CameraPreview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TakeFragment extends BaseFragment
{

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private Camera mCamera;
    private CameraPreview mPreview;

    private FrameLayout previewArea;

    // region PICTURE CALLBACK

    private Camera.PictureCallback mPicture = new Camera.PictureCallback()
    {

        @Override
        public void onPictureTaken(byte[] data, Camera camera)
        {
            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);

            if (pictureFile == null)
            {
                APIHelper.log('e', "Error creating media file, check storage permissions");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                APIHelper.log('e', "File not found: " + e.getMessage());
            } catch (IOException e) {
                APIHelper.log('e', "Error accessing file: " + e.getMessage());
            }


            ((PhotoActivity)TakeFragment.this.getActivity()).goToTitleScreen(pictureFile.getPath());
            //mCamera.startPreview();
        }
    };

    // endregion

    // region MAIN CALLBACKS

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frg_take_picture, container, false);

        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_IMMERSIVE);

        // Create an instance of Camera
        this.mCamera = APICameraHelper.getInstance();

        // Create our Preview view and set it as the content of our activity.
        this.mPreview = new CameraPreview(this.getActivity(), mCamera);
        this.previewArea = (FrameLayout)v.findViewById(R.id.camera_preview);
        this.previewArea.addView(mPreview);

        ImageButton flashButton = (ImageButton)v.findViewById(R.id.flashlight_button);
        ImageButton toggleCameraButton = (ImageButton)v.findViewById(R.id.camera_switch_button);
        ImageButton takePictureButton = (ImageButton)v.findViewById(R.id.take_pic_button);

        // region CAMERA OPTIONS BUTTONS

        takePictureButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                TakeFragment.this.mCamera.takePicture(null, null, mPicture);
            }
        });

        flashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                TakeFragment.this.toggleFlash(v);
            }
        });


        if(Camera.getNumberOfCameras() == 1)
        {
            toggleCameraButton.setVisibility(View.INVISIBLE);
        }
        else
        {
            toggleCameraButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    TakeFragment.this.toggleCamera();
                }
            });
        }

        // endregion

        // region NAVIGATION BUTTONS

        ImageButton myBottlesButton = (ImageButton)v.findViewById(R.id.my_bottles_button);
        ImageButton discoverButton = (ImageButton)v.findViewById(R.id.discover_button);

        myBottlesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent in = new Intent(TakeFragment.this.getActivity(), MyBottlesActivity.class);
                startActivity(in);
            }
        });

        discoverButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent in = new Intent(TakeFragment.this.getActivity(), DiscoverActivity.class);
                startActivity(in);
            }
        });

        // endregion

        return v;
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        this.mPreview.stopPreviewAndFreeCamera();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        this.mPreview.stopPreviewAndFreeCamera();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        this.mPreview.stopPreviewAndFreeCamera();
    }

    // endregion

    // region CAMERA FEATURES METHODS

    public void toggleFlash(View v)
    {

        if(this.mPreview.toggleFlashled())
        {
            v.setBackground(this.getActivity().getResources().getDrawable(R.drawable.ic_flashled));
        }
        else
        {
            v.setBackground(this.getActivity().getResources().getDrawable(R.drawable.ic_flashled_off));
        }
    }

    public void toggleCamera()
    {
        this.mPreview.toggleCamera();
    }

    // endregion

    // region UTILITY METHODS

    public static Uri getOutputMediaFileUri(int type)
    {
        /** Create a file Uri for saving an image or video */
        return Uri.fromFile(getOutputMediaFile(type));
    }


    public static File getOutputMediaFile(int type)
    {
        /** Create a File for saving an image or video */

        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Bottles");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists())
        {
            if (! mediaStorageDir.mkdirs())
            {
                APIHelper.log('e', "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        if (type == MEDIA_TYPE_IMAGE)
        {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "Bottle_"+ timeStamp + ".jpg");
        }
        else if(type == MEDIA_TYPE_VIDEO)
        {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "Bottle_"+ timeStamp + ".mp4");
        }
        else
        {
            return null;
        }

        return mediaFile;
    }

    // endregion
}
