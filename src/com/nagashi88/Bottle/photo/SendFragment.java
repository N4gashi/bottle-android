package com.nagashi88.Bottle.photo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.nagashi88.Bottle.BaseFragment;
import com.nagashi88.Bottle.R;
import com.nagashi88.Bottle.other.APIHelper;
import com.nagashi88.Bottle.tasks.TUploadBottle;

import java.io.File;

public class SendFragment extends BaseFragment
{

    private ProgressBar progressBar;
    private ImageButton sendButton;
    private String bottlePath;
    private EditText imageTitle;

    // region MAIN CALLBACKS

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.frg_send_picture, container, false);

        this.bottlePath = this.getArguments().getString(PhotoActivity.PHOTO_TAKEN_URI);
        this.sendButton = (ImageButton)v.findViewById(R.id.send_button);
        ImageView imageTaken = (ImageView)v.findViewById(R.id.photo_taken);
        this.imageTitle = (EditText)v.findViewById(R.id.title_image);
        this.progressBar = (ProgressBar)v.findViewById(R.id.progress_bar);

        imageTaken.setImageBitmap(this.getImageFromPath());
        this.sendButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SendFragment.this.startUpload();
            }
        });

        return v;
    }

    // endregion

    // region CUSTOM METHODS

    public Bitmap getImageFromPath()
    {
        String path = this.getArguments().getString(((PhotoActivity)this.getActivity()).PHOTO_TAKEN_URI);

        File imgFile = new File(path);
        Bitmap myBitmap = null;

        if(imgFile.exists())
        {
            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }

        return myBitmap;
    }

    public void startUpload()
    {
        this.sendButton.setBackground(this.getResources().getDrawable(R.drawable.ic_empty));
        this.sendButton.setEnabled(false);
        this.progressBar.setVisibility(View.VISIBLE);
        if(this.bottlePath != null)
        {
            Bitmap bm = BitmapFactory.decodeFile(this.bottlePath);
            new TUploadBottle(this, null, bm, this.imageTitle.getText().toString()).execute("/users/uploadAndSharePicture.json");
        }
        else
        {
            APIHelper.makeToast(this.getActivity(), getString(R.string.server_error));
        }
    }

    public void uploadFinished(boolean success)
    {
        this.sendButton.setClickable(false);
        this.progressBar.setVisibility(View.INVISIBLE);

        if(success)
        {
            this.sendButton.setBackground(this.getResources().getDrawable(R.drawable.ic_sucess));
        }
        else
        {
            this.sendButton.setBackground(this.getResources().getDrawable(R.drawable.ic_failed));
        }
    }

    // endregion

}
